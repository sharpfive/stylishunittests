//
//  ApplicationType.swift
//  StylishUnitTests
//
//  Created by Jaim Zuber on 4/12/16.
//  Copyright © 2016 Sharp Five Software. All rights reserved.
//

import Foundation
import UIKit

protocol ApplicationType {
    func openURL(_ url: URL) -> Bool
}

extension UIApplication : ApplicationType {
    // Don't need anything here
}
