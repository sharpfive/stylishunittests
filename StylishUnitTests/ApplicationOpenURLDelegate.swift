//
//  ApplicationOpenURLDelegate.swift
//  StylishUnitTests
//
//  Created by Jaim Zuber on 4/12/16.
//  Copyright © 2016 Sharp Five Software. All rights reserved.
//

import Foundation
import UIKit

class ApplicationOpenURLDelegate {
    
    var application: ApplicationType = UIApplication.shared
    
    func openURL(_ url: URL) {
        _ = application.openURL(url)
    }
}
