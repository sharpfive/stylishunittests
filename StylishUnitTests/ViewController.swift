//
//  ViewController.swift
//  StylishUnitTests
//
//  Created by Jaim Zuber on 4/7/16.
//  Copyright © 2016 Sharp Five Software. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    // dependencies
    var prizeNetworkClient: PrizeNetworkClientType = PrizeNetworkClient()
    var claimPrizeDelegate = ClaimPrizeDelegate()
    
    // For opening URL
    var openURLDelegate: ApplicationOpenURLDelegate = ApplicationOpenURLDelegate()
    var openURL = URL(string:"www.someurl.com")!
    
    @IBOutlet weak var thankYouLabel: UILabel!
    @IBOutlet weak var pressTheButtonButton: UIButton!
    @IBOutlet weak var claimPrizeButton: UIButton!
    
    @IBAction func pressTheButtonPressed(_ sender: AnyObject) {
        
        prizeNetworkClient.getPrizeStatusFromServer(onSuccess: { (didWinPrize) in
            self.thankYouLabel.isHidden = false
            
            if (didWinPrize) {
                
                self.thankYouLabel.text = "You Won!"
                self.claimPrizeButton.isHidden = false
            } else {
                
                self.thankYouLabel.text = "Sorry, you didn't win."
            }
            
    
        }, onError: { (error) in
            
            self.showErrorState()
        })
    }
    
    @IBAction func claimPrizeButtonPressed(_ sender: AnyObject) {
        
        claimPrizeDelegate.claimPrizeButtonPressedWithViewController(self)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prizeNetworkClient.doSomethingDuringViewDidLoad()
    }
    
    func showErrorState() {
        // Do some error handling
    }
}

