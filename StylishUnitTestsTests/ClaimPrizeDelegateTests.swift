//
//  ClaimPrizeDelegateTests.swift
//  StylishUnitTests
//
//  Created by Jaim Zuber on 4/10/16.
//  Copyright © 2016 Sharp Five Software. All rights reserved.
//

import XCTest
@testable import StylishUnitTests

class ClaimPrizeDelegateTests: XCTestCase {

    var sut: ClaimPrizeDelegate!
    var viewController: ViewControllerDouble!
    
    override func setUp() {
        super.setUp()
        sut = ClaimPrizeDelegate()
        viewController = ViewControllerDouble()
    }
        
    func testButtonPressPresentsCorrectSegue() {
        sut.claimPrizeButtonPressedWithViewController(viewController)
        
        XCTAssertEqual(viewController.performSegueWithIdentifiedCalledParameters?.identifier, "WonPrizeSegue")
    }
}

class ViewControllerDouble: UIViewController {
    
    var performSegueWithIdentifierCalled = false
    var performSegueWithIdentifiedCalledParameters: (identifier: String, sender: AnyObject?)?
    
    override func performSegue(withIdentifier identifier: String, sender: AnyObject?) {
        performSegueWithIdentifierCalled = true
        performSegueWithIdentifiedCalledParameters = (identifier, sender)
    }
}
