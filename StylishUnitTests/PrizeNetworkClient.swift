//
//  PrizeNetworkClient.swift
//  StylishUnitTests
//
//  Created by Jaim Zuber on 4/9/16.
//  Copyright © 2016 Sharp Five Software. All rights reserved.
//

import Foundation

protocol PrizeNetworkClientType {
    func getPrizeStatusFromServer(onSuccess: (Bool) -> Void, onError: (NSError) -> Void)
    func doSomethingDuringViewDidLoad()
}

class PrizeNetworkClient : PrizeNetworkClientType {
    func getPrizeStatusFromServer(onSuccess: (Bool) -> Void, onError: (NSError) -> Void ) {
        
        onSuccess(true)
    }
    
    func doSomethingDuringViewDidLoad() {
        
    }
}
