//
//  ViewControllerLoadTests.swift
//  StylishUnitTests
//
//  Created by Jaim Zuber on 4/12/16.
//  Copyright © 2016 Sharp Five Software. All rights reserved.
//

import XCTest
@testable import StylishUnitTests

class ViewControllerLoadTests: XCTestCase {
    
    var storyboard: UIStoryboard!
    var sut: ViewController!
    var prizeNetworkClient: PrizeNetworkClientDouble!

    override func setUp() {
        super.setUp()
        
        storyboard = UIStoryboard(name: "Main", bundle: nil)
        sut = storyboard.instantiateViewController(withIdentifier: "ViewController") as? ViewController
        
        // Don't create the in setup
        //let _ = sut.view
        
        // hydrate and inject dependencies
        prizeNetworkClient = PrizeNetworkClientDouble()
        sut.prizeNetworkClient = prizeNetworkClient

    }
    
    func testSomethingHappenedInViewDidLoad() {
        
        // Create the view during the test
        let _  = sut.view
        
        XCTAssertTrue(prizeNetworkClient.doSomethingDuringViewDidLoadCalled)
    }
}
