//
//  ApplicationOpenURLDelegateTests.swift
//  StylishUnitTests
//
//  Created by Jaim Zuber on 4/12/16.
//  Copyright © 2016 Sharp Five Software. All rights reserved.
//

import XCTest
@testable import StylishUnitTests

class ApplicationOpenURLDelegateTests: XCTestCase {

    var sut: ApplicationOpenURLDelegate!
    var application: ApplicationDouble!
    
    let urlToTest = URL(string:"https://www.someurl.com")!
    
    override func setUp() {
        super.setUp()
        sut = ApplicationOpenURLDelegate()
        
        application = ApplicationDouble()
        sut.application = application
    }

    func testOpensURL() {
        
        sut.openURL(urlToTest)
        
        XCTAssertTrue(application.openURLCalled)
    }
}

class ApplicationDouble : ApplicationType {

    var openURLCalled = false
    
    var openURLReturnValue = false
    
    func openURL(_ url: URL) -> Bool {
        openURLCalled = true
        return openURLReturnValue
    }
}
