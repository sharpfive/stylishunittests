//
//  TempConverter.swift
//  StylishUnitTests
//
//  Created by Jaim Zuber on 4/7/16.
//  Copyright © 2016 Sharp Five Software. All rights reserved.
//

import Foundation

class TempConverter {
    
    func convertFahrenheitToCelcius(_ fahrenheitTemperature: Double) -> Double {
        return (fahrenheitTemperature - 32) * 5.0/9
    }
}
