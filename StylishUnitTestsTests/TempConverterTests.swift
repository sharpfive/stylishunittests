//
//  TempConverterTests.swift
//  StylishUnitTests
//
//  Created by Jaim Zuber on 4/8/16.
//  Copyright © 2016 Sharp Five Software. All rights reserved.
//

import XCTest
@testable import StylishUnitTests

class TempConverterTests: XCTestCase {
    
    var sut: TempConverter!
    
    override func setUp() {
        super.setUp()
        sut = TempConverter()
    }
    
    func testFreezingFtoC() {
        XCTAssertEqual(sut.convertFahrenheitToCelcius(32), 0)
    }
    
    func testHotFtoC() {
        XCTAssertEqual(sut.convertFahrenheitToCelcius(95), 35)
    }
    
    func testColdFtoC() {
        XCTAssertEqualWithAccuracy(sut.convertFahrenheitToCelcius(45), 7.22, accuracy: 0.01)
    }
}
