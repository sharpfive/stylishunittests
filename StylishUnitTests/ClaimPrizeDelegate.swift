//
//  ClaimPrizeDelegate.swift
//  StylishUnitTests
//
//  Created by Jaim Zuber on 4/10/16.
//  Copyright © 2016 Sharp Five Software. All rights reserved.
//

import Foundation
import UIKit

class ClaimPrizeDelegate {
    
    func claimPrizeButtonPressedWithViewController(_ viewController: UIViewController) {
        
        viewController.performSegue(withIdentifier: "WonPrizeSegue", sender: viewController)
    }
}
