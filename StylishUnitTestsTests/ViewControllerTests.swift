//
//  ViewControllerTests.swift
//  StylishUnitTests
//
//  Created by Jaim Zuber on 4/9/16.
//  Copyright © 2016 Sharp Five Software. All rights reserved.
//

import XCTest
@testable import StylishUnitTests

class ViewControllerTests: XCTestCase {
    
    var storyboard: UIStoryboard!
    var sut: ViewController!
    var prizeNetworkClient: PrizeNetworkClientDouble!
    var claimPrizeDelegate: ClaimPrizeDelegateDouble!
    
    override func setUp() {
        super.setUp()
        storyboard = UIStoryboard(name: "Main", bundle: nil)
        sut = storyboard.instantiateViewController(withIdentifier: "ViewController") as? ViewController
        let _ = sut.view
        
        // hydrate and inject dependencies
        prizeNetworkClient = PrizeNetworkClientDouble()
        sut.prizeNetworkClient = prizeNetworkClient
        
        claimPrizeDelegate = ClaimPrizeDelegateDouble()
        sut.claimPrizeDelegate = claimPrizeDelegate
    }
    
    func testSanity() {
        XCTAssertNotNil(sut)
    }
    
    // MARK: Test initial conditions
    
    func testPressTheButtonExists() {
        XCTAssertNotNil(sut.pressTheButtonButton)
    }
    
    func testThankYouButtonExists() {
        XCTAssertNotNil(sut.thankYouLabel)
    }
    
    func testThankYouLabelIsHiddenOnStartup() {
        XCTAssertTrue(sut.thankYouLabel.isHidden)
    }
    
    // MARK: Setup network calls
    
    func testSuccessfulNetworkCallWithPrize() {
        pressButton(withNetworkSuccess:true, didWinPrize: true)

        XCTAssertFalse(sut.thankYouLabel.isHidden)
    }
    
    func testSuccessfulNetworkCallWithPrizeText() {
        pressButton(withNetworkSuccess:true, didWinPrize: true)
        
        XCTAssertEqual(sut.thankYouLabel.text, "You Won!")
    }

    func testSuccessfulNetworkCallNoPrize() {
        pressButton(withNetworkSuccess:true, didWinPrize: false)
        
        XCTAssertFalse(sut.thankYouLabel.isHidden)
    }
    
    func testSuccessfulNetworkCallNoPrizeTextMatches() {
        pressButton(withNetworkSuccess:true, didWinPrize: false)
        
        XCTAssertEqual(sut.thankYouLabel.text, "Sorry, you didn't win.")
    }
    
    func testClaimButtonPressCallsDelegate() {
        
        sut.claimPrizeButtonPressed(sut.claimPrizeButton)
        
        XCTAssertTrue(claimPrizeDelegate.claimPrizeButtonPressedWithViewControllerCalled)
    }
    
    // MARK: Helpers
    
    func pressButton(withNetworkSuccess networkSuccess: Bool, didWinPrize: Bool) {
        
        prizeNetworkClient.callResultsInSuccess = networkSuccess
        prizeNetworkClient.didWinPrize = didWinPrize
        
        // Act
        sut.pressTheButtonPressed(sut.pressTheButtonButton)
    }
}

class ClaimPrizeDelegateDouble: ClaimPrizeDelegate {
    
    var claimPrizeButtonPressedWithViewControllerCalled = false
    
    override func claimPrizeButtonPressedWithViewController(_ viewController: UIViewController) {
        claimPrizeButtonPressedWithViewControllerCalled = true
    }
}

class PrizeNetworkClientDouble: PrizeNetworkClientType {
    
    var callResultsInSuccess = false
    var didWinPrize = false
    
    var doSomethingDuringViewDidLoadCalled = false
    
    var doSomethingWithAParameterURL: URL?
    var doSomethingWithMultipleParametersParameters:(someURL: URL, someString: String)?
    
    func getPrizeStatusFromServerWithSuccess(_ onSuccess: (Bool) -> Void, onError: (NSError) -> Void) {
        
        if callResultsInSuccess {
            onSuccess(didWinPrize)
        }
    }
    
    func doSomethingDuringViewDidLoad() {
        doSomethingDuringViewDidLoadCalled = true
    }
    
    func doSomethingWithAParameter(_ someURL: URL) {
        doSomethingWithAParameterURL = someURL
    }
    
    func doSomethingWithMultipleParameters(_ someURL: URL, someString: String) {
        
        doSomethingWithMultipleParametersParameters = (someURL, someString)
    }
}
